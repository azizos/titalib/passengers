# Passengers

## PASSENGERS API

As a part of the TitaLib application, our Passengers API, written in Go, is used to view or modify passengers abroad the Titanic (only survivors who can borrow books!).

### What does this service do?

_Passengers API_ is a [RESTful](https://en.wikipedia.org/wiki/Representational_state_transfer) API helping you to view or update information about Titanic survivors.
The service is written in Go and offers several HTTP endpoints (described below) using [Gin](https://github.com/gin-gonic/gin) framework. To persist data, we're using a Mongo DB database.

In this repository, we're using [_passengers-collector_](https://gitlab.com/azizos/titalib/passengers-collector) project as a git _submodule_. In order to run the application locally, we'll have to work with a pre-populated database. Therefore, using `docker-compose`, we build and run an image of `passengers-collector` (using its `Dockerfile` in the submodule folder). 
Once the `docker-compose up` command is executed (_see below_), initially a Mongo database will run (in a container) and then the passengers-collector container will run and store passenger objects. After that, we can simply use our passengers API to interact with that already populated database.

Below a sample Passenger object:

```json
{
    "_id": "5ea5a4d3445d8670a4550c69",
    "passengerClass": 3,
    "name": "Miss. Margaret Delia Devaney",
    "sex": "female",
    "age": 19,
    "siblingsOrSpousesAboard": 0,
    "parentsOrChildrenAboard": 0,
    "fare": 7.8792
}
```

#### Available endpoints

| Method | Path               | Description                               |
| ------ | ------------------ | ----------------------------------------  |
| GET    | /passengers             | To retrieve a list of passengers               |
| GET    | /passengers/[id]        | To retrieve a passenger by its __id_ property  |
| PUT    | /passengers/[id]        | To update a passenger by its _id property      |
| DELETE | /passengers/[id]        | To delete a passenger by its _id property      |

#### Postman collection

*todo*

## Run
To run this service locally, you can choose to use `minikube` (k8s manifest in the `env` repository) or you could use `docker-compose` (recommended). 

Run the command below in this project's root directory and the API is locally available on port 8080 (by default) :)
-`docker-compose up --build --remove-orphans`