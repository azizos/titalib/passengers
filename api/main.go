package main

import (
	"context"
	"fmt"
	"os"

	"github.com/ardanlabs/conf"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"gitlab.com/azizos/titalib/passengers/api/common"
	api "gitlab.com/azizos/titalib/passengers/api/web"
	"go.mongodb.org/mongo-driver/mongo"
)

var dbCollectionName string = "passengers"
var log, _ = common.GetLogModule()
var db *mongo.Database
var ctx context.Context

func main() {
	if err := run(); err != nil {
		log.Error("Service cannot be started: ", err)
		os.Exit(1)
	}
}

func run() error {
	var cfg struct {
		MongoURI string `conf:"default:mongodb://localhost:27017"`
		MongoDB  string `conf:"default:tatalib"`
		APIHost  string `conf:"default:0.0.0.0:8080"`
		GinMode  string `conf:"default:debug"`
	}

	if err := conf.Parse(os.Args[1:], "Passengers", &cfg); err != nil {
		if err == conf.ErrHelpWanted {
			usage, err := conf.Usage("Passengers", &cfg)
			if err != nil {
				log.Error("Generating config usage: ", err)
			}
			fmt.Println(usage)
			return nil
		}
		return errors.Wrap(err, "Problem parsing api configs")
	}

	defer log.Info("Main: completed")

	db, ctx = common.ConnectDB(cfg.MongoURI, cfg.MongoDB)
	collection := db.Collection(dbCollectionName)

	gin.SetMode(cfg.GinMode)
	router := gin.Default()
	api.GetRoutes(router, collection)

	err := router.Run(cfg.APIHost)
	return err
}
