package models

// Passenger struct is used to store and parse passenger objects
type Passenger struct {
	ID                      string  `bson:"_id,omitempty" json:"_id,omitempty"`
	Pclass                  int     `json:"passengerClass" bson:"Pclass"`
	Name                    string  `json:"name" bson:"Name"`
	Sex                     string  `json:"sex" bson:"Sex"`
	Age                     float64 `json:"age" bson:"Age"`
	SiblingsOrSpousesAboard int     `json:"siblingsOrSpousesAboard" bson:"Siblings/Spouses Aboard"`
	ParentsOrChildrenAboard int     `json:"parentsOrChildrenAboard" bson:"Parents/Children Aboard"`
	Fare                    float64 `json:"fare" bson:"Fare"`
}
