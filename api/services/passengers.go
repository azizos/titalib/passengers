package services

import (
	"context"
	"time"

	"gitlab.com/azizos/titalib/passengers/api/models"

	"gitlab.com/azizos/titalib/passengers/api/common"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2/bson"
)

var log, _ = common.GetLogModule()
var ctx context.Context
var ctxCancelFunc context.CancelFunc
var timeTillContextDeadline = 2 * time.Second

// ListPassengers retrieves all Passenger objects from the database
func ListPassengers(collection *mongo.Collection) []*models.Passenger {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	var passengers []*models.Passenger
	cursor, err := collection.Find(ctx, bson.M{})
	defer cursor.Close(ctx)
	if err != nil {
		log.Debug("No passengers returned from the database:", err)
	} else {
		for cursor.Next(context.TODO()) {
			var result *models.Passenger
			err := cursor.Decode(&result)
			if err != nil {
				log.Debug("Problem retrieving passenger object:", err)
			} else {
				passengers = append(passengers, result)
			}
		}
	}
	return passengers
}

// GetPassenger finds one single passenger object based on id
func GetPassenger(collection *mongo.Collection, id string) *models.Passenger {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	var result *models.Passenger
	objectID, _ := primitive.ObjectIDFromHex(id)
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&result)
	if err != nil {
		log.Errorf("Problem finding a document: %v", err)

	}
	return result
}

// UpdatePassenger finds one single passenger object based on id
func UpdatePassenger(collection *mongo.Collection, id string, newPassengerObject *models.Passenger) *models.Passenger {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	objectID, _ := primitive.ObjectIDFromHex(id)
	var result *models.Passenger
	filterQuery := bson.M{"_id": objectID}
	res := collection.FindOneAndReplace(ctx, filterQuery, newPassengerObject)
	if res.Err() != nil {
		log.Errorf("Problem updating a document: %v: %v", id, res.Err)

	}
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&result)
	if err != nil {
		log.Errorf("Problem finding a document: %v", err)
	}
	return result
}

// DeletePassenger delete one single passenger object based on id
func DeletePassenger(collection *mongo.Collection, id string) error {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	objectID, _ := primitive.ObjectIDFromHex(id)

	res := collection.FindOneAndDelete(ctx, bson.M{"_id": objectID})
	if res.Err() != nil {
		log.Errorf("Problem removing a document: %v: %v", objectID, res.Err)
	}
	return res.Err()
}
