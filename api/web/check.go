package api

import "github.com/gin-gonic/gin"

// Check provides support for orchestration health checks.
type Check struct {
}

// Health validates the service is healthy and ready to accept requests.
func (c *Check) Health(context *gin.Context) {
	var health struct {
		Status string `json:"status"`
	}

	health.Status = "ok"
	context.JSON(200, health)
	return
}
