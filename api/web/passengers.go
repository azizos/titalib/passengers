package api

import (
	"gitlab.com/azizos/titalib/passengers/api/common"
	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/azizos/titalib/passengers/api/models"
	"gitlab.com/azizos/titalib/passengers/api/services"

	"github.com/gin-gonic/gin"
)

var log, _ = common.GetLogModule()

// List is used to retrieve a list of passengers objects
func List(context *gin.Context) {
	db := context.MustGet("db").(*mongo.Collection)
	passengers := services.ListPassengers(db)
	if len(passengers) <= 0 {
		context.AbortWithStatus(204)
		return
	}
	context.JSON(200, passengers)
	return
}

// Get is used to retrieve a passenger object by its id property: responding to /:id
func Get(context *gin.Context) {
	db := context.MustGet("db").(*mongo.Collection)
	id := context.Param("id")
	passenger := services.GetPassenger(db, id)
	if passenger == nil {
		context.AbortWithStatus(204)
		return
	}
	context.JSON(200, passenger)
	return
}

// Update is used to update a passenger object by its _id property
func Update(context *gin.Context) {
	var passenger *models.Passenger
	db := context.MustGet("db").(*mongo.Collection)
	id := context.Param("id")
	if err := context.ShouldBindJSON(&passenger); err != nil {
		context.AbortWithStatus(400)
		return
	}
	changedPassenger := services.UpdatePassenger(db, id, passenger)
	if changedPassenger == nil {
		context.AbortWithStatus(404)
		return
	}
	context.JSON(200, changedPassenger)
	return
}

// Delete is used to update a passenger object by its _id property
func Delete(context *gin.Context) {
	db := context.MustGet("db").(*mongo.Collection)
	id := context.Param("id")
	err := services.DeletePassenger(db, id)
	if err != nil {
		context.AbortWithStatus(400)
		return
	}
	context.Status(200)
	return
}
