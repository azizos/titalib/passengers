package api

import (
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
)

// GetRoutes defines and returns a list of routes to be used by the main HTTP handler
func GetRoutes(route *gin.Engine, db *mongo.Collection) {
	check := Check{}

	route.Use(gin.Recovery())
	route.Use(databaseMiddleware(db))

	passengers := route.Group("/passengers")
	{
		passengers.GET("/", List)
		passengers.GET("/:id", Get)
		passengers.PUT("/:id", Update)
		passengers.DELETE("/:id", Delete)
	}
	route.GET("/health", check.Health)
}

func databaseMiddleware(db *mongo.Collection) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("db", db)
		c.Next()
	}
}
