module gitlab.com/azizos/titalib/passengers

go 1.13

require (
	github.com/ardanlabs/conf v1.2.1
	github.com/gin-gonic/gin v1.5.0
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/pkg/errors v0.9.1
	go.mongodb.org/mongo-driver v1.3.2
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
